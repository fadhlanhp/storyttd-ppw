from django.shortcuts import render
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.core import serializers

from .forms import Search_Form
from django.views.decorators.csrf import csrf_exempt
import requests


def get_book(request):
    listbuku['status_code'] = 200
    if request.user.is_authenticated:
        count = request.session.get('counter', 0)
        request.session['counter'] = count
        for key, value in request.session.items():
            print('{} => {}'.format(key, value))
        
    return JsonResponse(listbuku)

@csrf_exempt
def index(request):
    response={}
    if request.user.is_authenticated:
        request.session['first_name'] = request.user.first_name
        request.session['last_name'] = request.user.last_name
        request.session['email'] = request.user.email
        if ('counter' in request.session.keys()):
            request.session['counter'] = request.session['counter']
            response['counter'] = request.session['counter']
        else:
            request.session['counter'] = 0
            response['counter'] = 0
        for key, value in request.session.items():
            print('{} => {}'.format(key, value))

    else:
        response['counter'] = 0
        
    response['searchform'] = Search_Form
    form = Search_Form(request.GET)
    global listbuku 


    if form.is_valid():   
        url = "https://www.googleapis.com/books/v1/volumes?q="
        url += request.GET['keyword']
        listbuku = requests.get(url).json()
        return render(request, 'index.html', response)
    else:
        url = "https://www.googleapis.com/books/v1/volumes?q=quilting"
        listbuku = requests.get(url).json()
        return render(request, 'index.html', response)

def loginnya(request):
    return render(request, 'login.html')

def tambah(request):
	request.session['counter'] = request.session['counter'] + 1
	return HttpResponse(request.session['counter'], content_type = "application/json")

def kurang(request):
	request.session['counter'] = request.session['counter'] - 1
	return HttpResponse(request.session['counter'], content_type = "application/json")

def logoutPage(request):
	request.session.flush()
	logout(request)
	return HttpResponseRedirect('/story9')

