from django import forms

class Search_Form(forms.Form):
    attrs = {
        'class': 'form-control',
        'placeholder': 'Keyword'
    }

    keyword = forms.CharField(label='Search', required=True, max_length=300, widget=forms.TextInput(attrs=attrs))
