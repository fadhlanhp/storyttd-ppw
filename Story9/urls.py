from django.conf.urls import url, include
from django.urls import path
from django.contrib.auth import views
from django.conf import settings
from .views import *

urlpatterns = [   
    path('', index, name='index'),
    path('get-book/', get_book, name="get_book"),
    path('tambah', tambah, name="tambah"),
	path('kurang', kurang, name="kurang"),
    path('login/', loginnya, name="login"),
    path('logout/', logoutPage, name="logout")
]


#856446014234-76fc0h955oifrq923i9biuhu7oducqpb.apps.googleusercontent.com id

#-3ZKidUeJdm9xwn09PstxEsH secret