from django.test import TestCase
from django.urls import resolve
from .views import index
from django.test import Client


class Story9Test(TestCase):
   
    def test_view_get_return_200(self):
        """
        Test request get return 200
        """
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_views_using_landing_func(self):
        """
        Test if a reverse using correct views function
        """
        found = resolve('/story9/')
        self.assertEqual(found.func, index)

    def test_get_correct_output_html(self):
        response = Client().get('/story9/')
        html_response = response.content.decode('utf8')
        self.assertIn("Google Books", html_response)

    def test_firing_get_json(self):
        response = Client().get('/story9/get-book')
        self.assertNotEqual(response.status_code, 200)

    

