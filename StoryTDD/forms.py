from django import forms

class Status_Form(forms.Form):
    attrs = {
        'class': 'form-control',
        'placeholder': 'Statusmu!'
    }

    statusnya = forms.CharField(label='Statusmu', required=True, max_length=300, widget=forms.TextInput(attrs=attrs))

class Subs_Form(forms.Form):
    attrsname = {
        'class' : 'form-control',
        'placeholder' : 'Masukan namamu',
        'id' : 'formNama'
    }

    attrsemail = {
        'class' : 'form-control',
        'placeholder' : 'Masukan emailmu',
        'id' : 'formEmail'
    }

    attrspassword = {
        'class' : 'form-control',
        'placeholder' : 'Masukan password',
        'type' : 'password',
        'id' : 'formPassword'
    }

    email = forms.EmailField(label='Email', required=True, max_length=30, widget=forms.TextInput(attrs=attrsemail))
    name = forms.CharField(label='Nama', required=True, max_length=40, widget=forms.TextInput(attrs=attrsname))
    password = forms.CharField(label='Password', required=True, max_length=30, widget=forms.TextInput(attrs=attrspassword))