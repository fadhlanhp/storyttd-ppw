from django.shortcuts import render
from .models import Status, Subscriber
from .forms import Status_Form, Subs_Form
from django.http import HttpResponseRedirect
from django.http import JsonResponse

def index(request):
    response = {}
    response['statusform'] = Status_Form

    allstatus = Status.objects.all()
    response['showstatus'] = allstatus
    form = Status_Form(request.POST)
    if request.method == 'POST' and form.is_valid():
        response['statusnya'] = request.POST['statusnya']   
        status = Status(statusnya=response['statusnya'])
        status.save()
        return render(request, 'landing_page.html', response)
    else:
        return render(request, 'landing_page.html', response)

def profile(request):
    response = {}
    response['subsform'] = Subs_Form
    return render(request, "profile.html", response)

def cek_email(request):
    tempCheck = Subscriber.objects.filter(email=request.POST['email'])

    if tempCheck:
        return JsonResponse({
            'pesan':'Email telah terpakai',
            'status':'fail'
        })
    
    return JsonResponse({
        'pesan':'Email bisa digunakan',
        'status':'success'
    })

def add_subs(request):
    if (request.method == "POST"):
        subscriber = Subscriber(
            email = request.POST['email'],
            nama = request.POST['name'],
            password = request.POST['password']   
        )
        subscriber.save()
        return JsonResponse({
            'pesan':'Selamat And telah Subscribe ke Website ini!'
        }, status = 200)
    else:
        return JsonResponse({
            'pesan':"Something wrong"
        }, status = 403)

def getsubs(request):
    data = list(Subscriber.objects.values())
    alldata = {
        'data' : data
    }

    return JsonResponse(alldata)

# Create your views here.
