from django.apps import AppConfig


class StorytddConfig(AppConfig):
    name = 'StoryTDD'
