from django.urls import path
from .views import *

urlpatterns = [   
    path('', index, name='landing_page'),
    path('profile', profile, name="showprofile"),
    path('cekemail/', cek_email, name="cekemail"),
    path('addsubs/', add_subs, name="addsubs"),
    path('get_subs_url', getsubs, name="getsubs")
]
