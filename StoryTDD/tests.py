from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from django.apps import apps
from .apps import StorytddConfig
from .models import Status
from .views import index, profile
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest


class StoryUnitTest(TestCase):
    def test_landing_page_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_profile_page_is_exist(self):
        response = Client().get('/profile')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_using_profile_func(self):
        found = resolve('/profile')
        self.assertEqual(found.func, profile)

    def test_landing_page_contains_hello(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<title>Hello!</title>', html_response)
        self.assertIn('<h1>Hello, Apa kabar?</h1>', html_response)

    def test_StoryTDD_using_landing_page_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'landing_page.html')
    
    def test_model_can_create_new_status(self):
        new_status = Status.objects.create(statusnya='Aku baik-baik saja')
        allstatus = Status.objects.all().count()
        self.assertEqual(allstatus,1)
    
    def test_can_save_a_POST_request(self):
        response = self.client.post('/', data = {'statusnya' : 'Sehat' })
        countstatus = Status.objects.all().count()
        self.assertEqual(countstatus,1)
        new_response = self.client.get('/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('Sehat', html_response)

    def test_apps(self):
        self.assertEqual(StorytddConfig.name, 'StoryTDD')
        self.assertEqual(apps.get_app_config('StoryTDD').name, 'StoryTDD')

class StatusTestCase(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.selenium = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
        super(StatusTestCase, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(StatusTestCase, self).tearDown()

    def test_status(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        status = selenium.find_element_by_id('id_statusnya')

        submit = selenium.find_element_by_id('submitbutton')

        status.send_keys('Coba Coba')

        submit.send_keys(Keys.RETURN)

        page = selenium.page_source
        self.assertIn('Coba Coba', page)
    
    def test_position_title(self):
        selenium = self.selenium
        selenium.get('http://ppw-b-lab-fadhlan.herokuapp.com')

        self.assertIn("Home", selenium.title)

    def test_position_opening(self):
        selenium = self.selenium
        selenium.get('http://ppw-b-lab-fadhlan.herokuapp.com')
        opening_text = selenium.find_element_by_tag_name('h2').text

        self.assertIn('Hi, Welcome to my personal Website!', opening_text)

    def test_css_textalign_opening(self):
        selenium = self.selenium
        selenium.get('http://ppw-b-lab-fadhlan.herokuapp.com')
        opening_text_textalign = selenium.find_element_by_tag_name('h2').value_of_css_property('text-align')

        self.assertIn('center', opening_text_textalign)

    def test_css_margintop_opening(self):
        selenium = self.selenium
        selenium.get('http://ppw-b-lab-fadhlan.herokuapp.com')
        opening_text_margintop = selenium.find_element_by_tag_name('h2').value_of_css_property('margin-top')

        self.assertIn('20px', opening_text_margintop)

