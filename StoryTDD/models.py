from django.db import models

class Status(models.Model):
    statusnya = models.CharField(max_length=300)
    created_at = models.DateTimeField(auto_now_add=True)

class Subscriber(models.Model):
    nama = models.CharField(max_length=40)
    email = models.EmailField()
    password = models.CharField(max_length=20)

# Create your models here.
