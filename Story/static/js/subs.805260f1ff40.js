var emailIsValid = false;

$(document).ready(function() {
    $("input").focusout(function() {
        checkAll();
    });
    
    $("#formEmail").keyup(function() {
        checkEmail();
    });
    
    $("#formPassword").keyup(function() {
        $('#statusForm').html('');
        if ($('#formPassword').val().length < 5) {
            $('#statusForm').append('<small style="color: red"> Password at least 5 character </small>');
        }
        checkAll();
    });

    $('input').focusout(function() {
        checkEmail()
    });

    $('#submit').click(function () {
        data = {
            'email' : $('#formEmail').val(),
            'nama' : $('#formNama').val(),
            'password' : $('#formPassword').val(),
            "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
        }
        $.ajax({
            type : 'POST',
            url : 'addsubs/',
            data : data,
            dataType : 'json',
            success : function(data) {
                alert(data['pesan']);
                document.getElementById('formEmail').value = '';
                document.getElementById('formNama').value = '';
                document.getElementById('formPassword').value = '';
                
                $('#statusForm').html('');
                checkAll();
            }
        })
    });
})



function checkEmail() {
    data = {
        'email':$('#formEmail').val(),
        "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
    }
    console.log('1');
    $.ajax({
        type: "POST",
        url: 'cekemail/',
        data: data,
        dataType: 'json',
        success: function(data) {
            $('#statusForm').html('');
            if (data['status'] === 'fail') {
                emailIsValid = false;
                $('#submit').prop('disabled', true);
                $('#statusForm').append('<small style="color:red">' + data["pesan"] + '</small>');
            } else {
                emailIsValid = true;
                checkAll();
                $('#statusForm').append('<small style="color:green">' + data["pesan"] + '</small>');
            }
            
        }
    });
}

function checkAll() {
    if (emailIsValid && 
        $('#formNama').val() !== '' && 
        $('#formPassword').val() !== '' &&
        $('#formPassword').val().length > 5) {
        
        $('#submit').prop('disabled', false);
    } else {
        $('#submit').prop('disabled', true);
    }
}

$(document).ready(function() {
    $.ajax({
      url: 'get_subs_url',
      type: 'GET',  
      context: document.body,
      success: function(data) {
        $.each(data, function(i, item) {
          $("#list-subs-html").append(
            `
            <tr>
              <td class='text bg-grey'>
                <img width='50px' src='${item}' alt=''>
              </td>
            </tr>
            
            `)
        });
      }
    });
  });

