function change(){
    var themeElement = document.getElementById('temanavbar')
    if (themeElement.className == 'BlueNavbar'){
        document.getElementById('temanavbar').className = 'GreenNavbar';
        document.getElementById('aboutme').className = 'deskripsi-diri-hijau';
        document.getElementById('temacv').className = 'cv-hijau';
    }
    else {
        document.getElementById('temanavbar').className = 'BlueNavbar';
        document.getElementById('aboutme').className = 'deskripsi-diri';
        document.getElementById('temacv').className = 'cv';
    }
}

$( function() {
    $( "#accordion" ).accordion();
  } );